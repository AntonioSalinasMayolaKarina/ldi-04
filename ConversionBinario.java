
package lenguajeinterfaz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author kary
 */

public class ConversionBinario
{
    BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
  
    void menu()throws IOException
    {
        while(true)
        {
        System.out.println("\nMENU");
   
        System.out.println("1.-BINARIO => HEXADECIMAL");    
        System.out.println("2.-HEXADECIMAL => BINARIO");
        System.out.print("3.-SALIR\n OPCION: ");
      
        int opc=Integer.parseInt(in.readLine());
        String valor;
        System.out.print("\n");
      
            switch(opc)
            {

                case 1:
                    System.out.println("\n\nBINARIO => HEXADECIMAL");
                    System.out.print("NumeroBINARIO= ");
                    valor=in.readLine();
                    bin_hex(valor);
                    break;
                
                case 2:
                    System.out.println("\n\nHEXADECIMAL => BINARIO");
                    System.out.print("NumeroHEXADECIMAL= ");
                    valor=in.readLine();
                    hex_bin(valor);
                    break;

                case 3:
                    System.exit(0);
                    break;
            }
        }
    }
  
    void bin_hex(String valor)
    {
        int dec=Integer.parseInt(valor,2);
        valor=Integer.toHexString(dec);
        System.out.print("HEXADECIMAL: "+valor);
    }
  
    void hex_bin(String valor)
    {
        int dec=Integer.parseInt(valor,16);
        valor=Integer.toBinaryString(dec);
        System.out.print("BINARIO: "+valor);
    }
  
  
    public static void main(String [] args) throws IOException
    {
        ConversionBinario ConversionBinario=new ConversionBinario();
        ConversionBinario.menu();
    }
}